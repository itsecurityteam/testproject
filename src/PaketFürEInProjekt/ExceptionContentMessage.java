package PaketFürEInProjekt;
import java.util.HashMap;

public class ExceptionContentMessage {

	private static HashMap<Integer, String> errors;
	
	private static ExceptionClass instance = ExceptionClass.getInstance();
	
	/*
	 * Im Singleton sind alle Konstruktoren private
	 */
	 ExceptionContentMessage(){
		this.errors = new HashMap<Integer,String>();
		
		this.initialize();	 
	}
	
	public synchronized  ExceptionClass getInstance(int erroCode){
		/*
		if (instance == null) {
            instance = ExceptionClass.getInstance((this.getError(erroCode));
        }
        */
        return this.instance;
	}	
	
	public void initialize(){
		 
		this.setError(302, "Error : You are already inside a room");
		this.setError(404, "Error : room not found");
		this.setError(502, "");
	}
	
	public HashMap getErrors(){		
		return this.errors;
	}
    
	public String getError(int key){		
		return this.errors.get(key);
	}
	
	public void setError(int key , String value){
		this.errors.put(key, value);
	}

}
