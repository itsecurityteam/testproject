package PaketFürEInProjekt;
import java.util.ArrayList;
import java.util.List;

public class Token {
	private int length;
	// Idee: könnte man die letters nicht aus einer Enumeration beziehen? <Martin>
	private String letters  ="0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
    public Token(int length) {
	 
		this.length = length;
	}

	public String shuffle(String input){  
        List<Character> characters = new ArrayList<Character>();
        for(char c:input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }
	
	public String generateToken(){
			
			String letterscontent = "";
		 
			for (int i = 0; i < length; i++) {
				letterscontent += this.letters;
			}
			String token = this.shuffle(letterscontent).substring(0, this.length);
			
			return token;
	}
	
}
