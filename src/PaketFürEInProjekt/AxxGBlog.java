package PaketFürEInProjekt;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import sun.misc.CharacterEncoder;

/**
 * Beispiel Klasse für ver- und entschlüsselung
 * Diese soll später NICHT mit abgegeben werden!!!!!
 * @author EM
 *
 */
public class AxxGBlog {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		String fileName = "logFile.txt";
		
		LogAppend la = new LogAppend(fileName);
		LogRead lr = new LogRead();
		

		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		//System.out.println("Your input is: "+input);
		
		
		
		SecretKeySpec tokenKey =  la.generateKey(input);
		Scanner in2 = new Scanner(System.in);
		String inputText = in2.nextLine();
		String verschlüsselterText = la.encrypting(inputText, tokenKey);
		System.out.println(verschlüsselterText);
		System.out.println("Versuche es zu entschlüsseln ohne Token zu kennen!");
		Scanner in3 = new Scanner(System.in);
		String inputText2 = in3.nextLine();
		SecretKeySpec ownKey =  la.generateKey(inputText2);
		String erg = la.decrypting(verschlüsselterText, ownKey);
		System.out.println(erg);
		
		
		/*
		// Das Passwort bzw der Schluesseltext
		String keyStr = input;
		// byte-Array erzeugen
		byte[] key = (keyStr).getBytes("UTF-8");
		// aus dem Array einen Hash-Wert erzeugen mit MD5 oder SHA
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key = sha.digest(key);
		// nur die ersten 128 bit nutzen
		key = Arrays.copyOf(key, 16); 
		// der fertige Schluessel
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
		

		// der zu verschl. Text
		String text = "Das ist der Text";

		// Verschluesseln
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encrypted = cipher.doFinal(text.getBytes());

		// bytes zu Base64-String konvertieren (dient der Lesbarkeit)
		BASE64Encoder myEncoder = new BASE64Encoder();
		String geheim = myEncoder.encode(encrypted);

		// Ergebnis
		System.out.println(geheim);
		
		// BASE64 String zu Byte-Array konvertieren
		BASE64Decoder myDecoder2 = new BASE64Decoder();
		byte[] crypted2 = myDecoder2.decodeBuffer(geheim);

		// Entschluesseln
		Cipher cipher2 = Cipher.getInstance("AES");
		cipher2.init(Cipher.DECRYPT_MODE, secretKeySpec);
		byte[] cipherData2 = cipher2.doFinal(crypted2);
		String erg = new String(cipherData2);

		// Klartext
		System.out.println(erg);
		*/

	}

}