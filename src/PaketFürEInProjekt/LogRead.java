package PaketFürEInProjekt;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;
//delete this commentar
/**
 * 
 * @author Ernesto&Co
 * LogRead als Singleton
 *
 */
public class LogRead {
	
	private static Person person;	
	private Room room;
//	private ArrayList<Person> persons;
	private ArrayList<Room> rooms;
	private static String R = "-R";
	private static String T = "-T";
	private static String S = "-S";
	private static String G = "-G";
	private static String E = "-E";
	private static Status EmployeeOrGuest;
	private static Read read;
	private String bufferContent;
    private static String pathFile;
    private static  Pattern pattern;
    private static  Matcher matcher;
    private HashMap  <String , String> inputErros;
    private static Gallery theGallery = new Gallery();
	private static final String USERNAME_PATTERN = "^[A-Za-z]{3,60}$";
    private static final String TOKEN_PATTERN = "^[A-Za-z0-9]{5,60}$";
    private static final String TIMSTAMP_PATTERN = "^[0-9]{1, 10}$";
		
    private static String fileContent;

    private static ExceptionClass exceptions = ExceptionClass.getInstance();
      
    public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
    	String[] input = {"-K","token","-S","C:\\Users\\EM\\Desktop\\keyTest.txt"};
    	
    	if(parseEntry(input)==0) {
    		getFileContent();
    		generateOutput(read);
    	}
    	else {
    		System.out.println("IRGENDWAS IST SCHIEFGELAUFEN");
    	}
    }
//	public LogRead(String pathFile) {			
//		this.pathFile = pathFile;
//	}	
	
	public static int parseEntry(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		System.out.println(args);
		for(int i = 0;  i< args.length; i++) {
			if(args[i].equals("-K")) {
				String token = args[i+1];
				SecretKeySpec key =  generateKey(token);
				String ersteFileZeile = decrypting(getFileContent(), key);
				/*
				 * if(!(validateToken(args[i+1]))) {
				 * System.out.println("Token is falsch");
					return 255;
				}
				 */
					
//				validateToken(args[i+1]);
				//validate TOKEN of Log and return value in a boolean;
			}
			if(args[i].equals(R)) {
				if(read == null) {
					read = Read.R;
				} else {
					return 255;
					//throw EXECPTION
				}
			}
			if(args[i].equals(S)) {
				if(read == null) {
					read = Read.S;
				} else {
					return 255;
				}
			}
			if(args[i].equals(T)) {
				if(read == null) {
					read = Read.T;
				} else {
					return 255;
					//throw EXECPTION oder Umimplemented
				}
			}
			
//			if(args[i].equals(E)) {
//				if(read == null) {
//					read = Read.R;
//				} else {
//					return 255;
//					//throw EXECPTION
//			}
//			
//			if(args[i].equals(G)) {
//				if(read == null) {
//					read = Read.R;
//				} else {
//					return 255;
//					//throw EXECPTION
//				}
//			}
		}
		pathFile = args[args.length-1];
		return 0;
		}
//	}
	
	public static void readLogLine(String line) {
		Person person = null;
		Room room;
		String[] arguments = line.split(" ");
		String personName, roomId;
		Status PersonStatus;

		if(arguments.length == 8) {
			personName = arguments[6];
			if(arguments[5].equals("-G")) { PersonStatus = Status.GUEST; }
			else { PersonStatus = Status.EMPLOYEE; }
			person = new Person(personName,PersonStatus);
			theGallery.addPerson(person);
			System.out.println("person "+ person.getName() +" in gallery");
			
		}

		if(arguments.length == 10) {
			//create Person and add to List persons
			personName = arguments[6];
			roomId = arguments[8];
			if(arguments[5].equals("-G")) { PersonStatus = Status.GUEST; }
			else { PersonStatus = Status.EMPLOYEE; }
			
//			person = new Person(personName, PersonStatus);
			room = new Room(Integer.parseInt(roomId));
			
			if(theGallery.getPersons().size() >= 1) {
				for(Person p : theGallery.getPersons()) { 
					if(personName.equals(p.getName()) && PersonStatus.equals(p.getStatus())) { //person already in Gallery
						System.out.println("person schon in gallery " + p.getName());
						for(Room r : theGallery.getRooms()) {
							if(r.getRoomID() == room.getRoomID()) {  //room already exists
								if(r.appendPerson(p)==0) {   //add person if not already in room
									p.addVisitedRooms(r.getRoomID());
									System.out.println("person " + p.getName() + " jetzt in room : "+ r.getRoomID());
									System.out.println("room "+r.getRoomID() + " now hat "+ r.getPersons());
									System.out.println("----------2--------");
									return;
								} else {
								System.out.println("just remove person");
								System.out.println("room "+r.getRoomID() + " now hat "+ r.getPersons());
								System.out.println("----------1--------");
								return;
								}
							}
						}
						
						//room doesnt exist
						room.appendPerson(p);
						p.addVisitedRooms(room.getRoomID());
						theGallery.addRoom(room);
						System.out.println("just created room");
						System.out.println("room "+room.getRoomID() + " now hat "+ room.getPersons());
						System.out.println("----------3--------");
						return;
					}
				}
			}
		}
	}
	
	public static void generateOutput(Read readTyp) {
		
		if(readTyp == Read.R) {
			// output for R (written by Kiko)
			for(Person p: theGallery.getPersons()) {
				if(p.getName().equals(person.getName())){
					ArrayList<Integer> visited_Rooms = p.getVisitedRooms();
					System.out.println("");
					for(int i=0; i < visited_Rooms.size(); i++){
						System.out.print(visited_Rooms.get(i));
						if(i<visited_Rooms.size()-1) { 
							System.out.print(",");
	                    }
	                }
	            }
			}		
		}
		
		if(readTyp == Read.S) {
			for(Person p : theGallery.getPersons()) {
				if(p.getStatus().equals(Status.EMPLOYEE)) {
					System.out.print(p.getName()+" ");
				}
			}
			System.out.println();
			for(Person p : theGallery.getPersons()) {
				if(p.getStatus().equals(Status.GUEST)) {
					System.out.print(p.getName()+" ");
				}
			}
			System.out.println();
			for(Integer i: theGallery.getRoomsId()) {
				System.out.print(i + " :");
				for(Room r: theGallery.getRooms() ) {
					if(r.getRoomID() == i) {
						for(Person per : r.getPersons()) {
							System.out.print(per.getName());
						}
					}
				}
				System.out.println();
			}
			
			
		}
		
		
		
		if(readTyp == Read.T) {
			System.out.println("Unimplemented");
		}
	}

	public static String getFileContent() {
		File logFile = new File(getPathFile());
		 if (!logFile.canRead() || !logFile.isFile()){
			 System.out.println(exceptions.getError(601));
			 System.exit(0);
		 }
	     BufferedReader in = null;
	     try {
	         in = new BufferedReader(new FileReader(logFile));
	         String zeile = null;
	         while ((zeile = in.readLine()) != null) {
	        	 System.out.println(zeile);
	            readLogLine(zeile);
	         }
	     } catch (IOException e) {
	         e.printStackTrace();
	     } finally {
	         if (in != null)
	             try {
	                 in.close();
	                } catch (IOException e) {
	                }
	        }
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public static String getPathFile() {
		return pathFile;
	}

	public void setPathFile(String pathFile) {
		LogRead.pathFile = pathFile;
	}
	
	 public void addLineToBuffer(String line){
	    	this.bufferContent += line + "\n";
	    	//this.setBufferContent(this.bufferContent += line );
	    }
	
	/**
     * Validate timestamp with regular expression
     * @param String timestamp   to validate with matched format
     * @return true valid token, false invalid token  
     */
	 public static boolean validateToken(String token){
		
		pattern = Pattern.compile(TOKEN_PATTERN);
		matcher = pattern.matcher(token);
		return matcher.matches();
	 }
	 
	 /*
	    * Method for generating a key using an String Input
	    */
	   public static SecretKeySpec generateKey(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		   byte[] key = (input).getBytes("UTF-8");
		   MessageDigest sha = MessageDigest.getInstance("SHA-256");
		   key = sha.digest(key);
		   key = Arrays.copyOf(key, 16);
		   SecretKeySpec secretKS = new SecretKeySpec(key, "AES");
		   return secretKS;			   
	   }
	   
	   /*
	    * Methode for encrypting String inputs by using a key
	    */
	   public String encrypting(String text, SecretKeySpec key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		   Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		   cipher.init(Cipher.ENCRYPT_MODE, key);
		   byte[] encrypted = cipher.doFinal(text.getBytes());
		   Base64.Encoder myEncoder = Base64.getEncoder();
		   String geheim = myEncoder.encodeToString(encrypted);
		   return geheim;
	   }
	   
	   /*
	    * Method for decrypting an encypted String 
	    */
	   public static String decrypting(String text, SecretKeySpec key) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		   Base64.Decoder myDecoder = Base64.getDecoder();
		   byte[] crypted2 = myDecoder.decode(text);
		   Cipher cipher2 = Cipher.getInstance("AES");
		   cipher2.init(Cipher.DECRYPT_MODE, key);
		   byte[] cipherData2 = cipher2.doFinal(crypted2);
		   String erg = new String(cipherData2);
		   return erg;
	   }
	 

}
