package PaketFürEInProjekt;

import java.util.ArrayList;
import java.util.List;

public class Room {
	private List<Person> persons = new ArrayList<Person>();
	ExceptionContentMessage errorMessage; 
	
	private int roomID;
	 
	public Room(int roomID) {
		super();
		this.roomID = roomID;
		errorMessage = new ExceptionContentMessage();
	}
	
	public int getRoomID() {
		return roomID;
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}
	
//	public void addPersonToRoom(Person p) {
//		
//	}
//	
	 
   public int appendPerson(Person person){
//	   System.out.println(person.getCurrentRoom());
//	   person.setCurrentRoom(this);
	   if(person.getCurrentRoom() == null){
		   person.setCurrentRoom(this);
		   persons.add(person);
	       return 0;
	   }
	   
	   else {
		   if(person.getCurrentRoom().equals(this)) {
	   			person.getCurrentRoom().removePerson(person);
			   	return 255;
		   }
		   else {
			   person.getCurrentRoom().removePerson(person);
			   person.setCurrentRoom(this);
			   persons.add(person);
			   return 0;
		   }
	   }
//	   if(!(person.getCurrentRoom().getRoomID() == this.roomID)){
//
//			      person.getCurrentRoom().removePerson(person);	   
//		  
//		   if(getPersons().add(person)){
//			   person.setCurrentRoom(this);
//			   return 0;
//		   }else if(person.getCurrentRoom().getRoomID() != this.roomID){
//			   //System.out.println("Your are already on the room ");
//		       return 225;
//	       }else
//			   
//			    return 225;
//	   }
//	   return 225;
   }
  
  public boolean removePerson(Person person){
	  if(getPersons().remove(person)){
	        person.setCurrentRoom(null);
	        return true;
      }else
	        return false;
	   
  }
  
  public boolean isInsideRoom(Room room, Person person){
	 	 
	 	 return room.getPersons().contains(person);
  }
	
  public void listingContentRoom(){
	  int index = 1;
	  System.out.println("RoomID = " + this.roomID +" *******");
	  for (Person person : persons) {
		  System.out.println(index +"--- " + person.getName());
		  index++;
	}
	  System.out.println( );
  }

	 
}
