package PaketFürEInProjekt;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.*;
import java.util.Base64;
import java.util.Base64.Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class LogAppend {
		private String bufferContent;
	    private String pathFile;
	    private  Pattern pattern;
	    private  Matcher matcher;
	    final static String TIMESTAMP = "-T";
		final static String TOKEN = "-K";
		final static String EMPLOYEE = "-E";
		final static String GUEST = "-G";
		final static String ROOM = "-R";
		final static String BATCH = "-B";
		final static String ARRIVE = "-A";
		final static String LEAVE = "-L";
		
        final static String [] matchersParameters = {"-T", "-K", "-E", "-G", "-A", "-L"};
        final static String [] optionsParameter = { "B", "-R"};
        
		private static String time, token, roomId, Name , logFile, arriveOrLeave, employeeOrGuest ;
	    private HashMap  <String , String> inputErros;
	    private static final String USERNAME_PATTERN = "^[A-Za-z]{3,60}$";
	    private static final String TOKEN_PATTERN = "^[A-Za-z0-9]{5,60}$";
	    private static final String TIMSTAMP_PATTERN = "^[0-9]{1,10}$";
	    private static final String PAHTFILE_PATTERN = "^[A-Za-z0-9_/.]{3,255}$";
	    
	  
	    ExceptionContentMessage errorMessage;
	    
	    /**
        * Constructor LogAppend
        **/
		public LogAppend(String pathFile) {
			this.pathFile = pathFile;
			this.inputErros = new HashMap<String,String>();
			this.bufferContent = "";
			errorMessage = new ExceptionContentMessage();
			this.initializeInputErrors();
		}
	
	    public HashMap<String, String> getInputErros(int key) {
			return inputErros;
		}

	    
		public String getInputError(int key){		
			return this.inputErros.get(key);
		}
		
		public void setInputError(String key , String value){
			this.inputErros.put(key, value);
		}

        public void setBufferContent(String bufferContent) {
            
            this.bufferContent = bufferContent;
        }
        
		public String getBufferContent() {
            
            return bufferContent;
        }
		public void addLineToBuffer(String line){
		    this.bufferContent += line + "\n";
		    //this.setBufferContent(this.bufferContent += line );
		}
	 
		 /**
	     * Validate username with regular expression
	     * @param username username for validation
	     * @return true valid username, false invalid username
	     */
	    public  boolean validateUsername(final String username){
	    	pattern = Pattern.compile(USERNAME_PATTERN);
	        matcher = pattern.matcher(username);
	        return matcher.matches();
	
	    }
	    
	    /**
	     * Validate ID of Room with regular expression
	     * @param String roomID for validation
	     * @return integer valid roomID when  matched format, -1 when the paramter doesnot match the Format
	     */
	    public int checkRoomId(String roomID)throws NumberFormatException {
	    	int id = -1;
			 try { if(this.validateTimestamp(roomID))
				    id  =  Integer.parseInt(roomID.replaceFirst("^0+(?!$)", ""));
				    
				  } catch (NumberFormatException e) {
					  
				    e.printStackTrace();
				  }
			 return id;
		 }
        
	    /**
	     * Validate timestamp with regular expression
	     * @param String timestamp   to validate with matched format
	     * @return true valid timestamp, false invalid timestamp when the format doesnot matched
	     */
	    public boolean validateTimestamp(String timestamp){
	    	pattern = Pattern.compile(TIMSTAMP_PATTERN);
	    	matcher = pattern.matcher(timestamp);
	    	return matcher.matches();
	    }
	     public Status validateStatus(char status) throws ExceptionClass {
		    	Status state;
		    	switch (status) {
				case 'E':
					state =  Status.EMPLOYEE;
					break;
		        case 'G':
		        	state =  Status.GUEST;
					break;
		
				default:
					throw errorMessage.getInstance(502);
				}
		    	 return state;
		    }
          
	     /**
	     * Validate timestamp with regular expression
	     * @param String timestamp   to validate with matched format
	     * @return true valid token, false invalid token  
	     */
		 public boolean validateToken(String token){
			
			pattern = Pattern.compile(TOKEN_PATTERN);
			matcher = pattern.matcher(token);
			return matcher.matches();
		 }
		 public boolean validatePatchFile(String file){// Path + filename
	            pattern = Pattern.compile(PAHTFILE_PATTERN);
	            matcher = pattern.matcher(file);
	            return matcher.matches();
	             
	         }
         
		 /**
	     * read the file width name filename  and store the Content  in the bufferContent
	     * @param String filename   to validate with matched format
	     */
		 public void readLogFile(String filename){
			    //Ready Section 
		        
		        // This will reference one line at a time
		        String line = null;
		        String lastLine =null;

		        try {
		            // FileReader reads text files in the default encoding.
		            FileReader fileReader = new FileReader(filename);

		            // Always wrap FileReader in BufferedReader.
		            BufferedReader bufferedReader = new BufferedReader(fileReader);

		            while((line = bufferedReader.readLine()) != null) {  
		                this.addLineToBuffer(line);
		            }   

		            // Always close files.
		            bufferedReader.close();         
		        } catch(IOException ex) {
		 
		             ex.printStackTrace();
		        }
				 
		   }
		 /**
	     *  Store the Content of buffeContent on the file width the name filename
	     * @param String filename   to validate with matched format
	     */
		   public void wirteLogFile(String filename){
			   try {
		           // Assume default encoding.
		           FileWriter fileWriter = new FileWriter(filename);
		
		           // Always wrap FileWriter in BufferedWriter.
		           BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		
		           // Note that write() does not automatically
		           // append a newline character.
		           bufferedWriter.write(this.getBufferContent());
		           
		           //bufferedWriter.newLine();
		            
		           // Always close files.
		           bufferedWriter.close();
		       }
		       catch(IOException ex) {
		
		            ex.printStackTrace();
		       }
		   }
		   
		   
		   /*
		    * Initialize all message Errors 
		    */
		   public void initializeInputErrors(){
				 
				this.setInputError("Timestamp", "invalid timsetamp number");
				this.setInputError("Token", "invalid Password please try again ");
				this.setInputError("Movement", "invalid movement please enter A for Arrive or L for leave");
				this.setInputError("Status", "invalid Status try E for employee or G for guest");
				this.setInputError("Username", "invalid Username: Names are alphabetic characters (a-z, A-Z) in upper and lower case");
				this.setInputError("Log", "invalid Path for your LogFile.");
				this.setInputError("Room", " invalid RoomID, enter a valid number for you Room.");
			}
		   
		   public int inputParser(String[] args){
			   
	             int errorCode = 255;
	                    if(args.length  == 2){
	                        if(!args[0].equals(BATCH) || args[1].length()< 3){
	                            return errorCode;
	                        }else return 0;
	                        
	                    }
	 
	                    for(int i=0 ; i < matchersParameters.length ; i++) {
	                        
	                         switch (matchersParameters[i]) {
	                         
	                            case TIMESTAMP:
	                                
	                                if(args.length > 2){
	                                    
	                                        if(!(args[0].equals(TIMESTAMP)) ||
	                                           !this.validateTimestamp(args[1]) ||
	                                           !(args[2].equals(TOKEN))    
	                                          ) return errorCode;
	                                         
	                                } else{
	                                    
	                                        return errorCode;
	                                }
	 
	                                break;
	                                
	                            case TOKEN:                             
	                                 
	                                if( args.length > 4 ){
	                                    
	                                        if(!args[2].equals(TOKEN) || !this.validateToken(args[3]))
	                                            return errorCode;
	                                        if(!(args[4].equals(GUEST) || args[4].equals(EMPLOYEE)))
	                                            return errorCode;
	                                }else{ 
	                                    return errorCode;
	                                }
	 
	                                break;
	                                
	                            case GUEST:        
	                            case EMPLOYEE:
	                                 
	                                 if(args.length > 6){
	                                      if(!(args[4].equals(GUEST) || args[4].equals(EMPLOYEE)))
	                                            return errorCode;
	                                      
	                                      if(!this.validateUsername(args[5]))
	                                            return errorCode;
	                                      if(!(args[6].equals(ARRIVE) || args[6].equals(LEAVE)))
	                                        return errorCode;
	                                 }else 
	                                     return errorCode;
	                                break;
	                                
	                            case ARRIVE:                                 
	                            case  LEAVE:
	                                
	                                if( args.length == 8 ){
	                                    
	                                    if(!(args[6].equals(ARRIVE) || args[6].equals(LEAVE)))
	                                            return errorCode;
	                                     
	                                    if(args[7].length() < 2 )
	                                            return errorCode;
	                                     
	                                    
	                                        
	                                }else if( args.length == 10 ){
	                                    if(!(args[6].equals(ARRIVE) || args[6].equals(LEAVE)))
	                                            return errorCode;
	                                    if(!args[7].equals(ROOM) || (this.checkRoomId(args[8]) == -1))
	                                              return errorCode;
	                                    if(args[9].length()< 2 )
	                                              return errorCode;
	                                }else 
	                                    
	                                    return errorCode;
	                                break;
	   
	                            default:
	                                break;
	          //                      
	                        }
	                }
	     
	               return 0;
	           }
		   
		   /*
		    * Method for generating a key using an String Input
		    */
		   public static SecretKeySpec generateKey(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException{
			   byte[] key = (input).getBytes("UTF-8");
			   MessageDigest sha = MessageDigest.getInstance("SHA-256");
			   key = sha.digest(key);
			   key = Arrays.copyOf(key, 16);
			   SecretKeySpec secretKS = new SecretKeySpec(key, "AES");
			   return secretKS;			   
		   }
		   
		   /*
		    * Methode for encrypting String inputs by using a key
		    */
		   public String encrypting(String text, SecretKeySpec key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
			   Cipher cipher = Cipher.getInstance("AES");
			   cipher.init(Cipher.ENCRYPT_MODE, key);
			   byte[] encrypted = cipher.doFinal(text.getBytes());
			   Base64.Encoder myEncoder = Base64.getEncoder();
			   String geheim = myEncoder.encodeToString(encrypted);
			   return geheim;
		   }
		   
		   /*
		    * Method for decrypting an encypted String 
		    */
		   public static String decrypting(String text, SecretKeySpec key) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
			   Base64.Decoder myDecoder = Base64.getDecoder();
			   byte[] crypted2 = myDecoder.decode(text);
			   Cipher cipher2 = Cipher.getInstance("AES");
			   cipher2.init(Cipher.DECRYPT_MODE, key);
			   byte[] cipherData2 = cipher2.doFinal(crypted2);
			   String erg = new String(cipherData2);
			   return erg;
		   }
    
	 
}
