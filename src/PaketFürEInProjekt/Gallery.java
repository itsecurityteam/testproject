package PaketFürEInProjekt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Gallery {
	private ArrayList<Person> persons = new ArrayList<Person>();
	static ArrayList<Integer> roomsId = new ArrayList<Integer>();
	private ArrayList<Room> rooms = new ArrayList<Room>();
	
	public void addPerson(Person person) {
		persons.add(person);
	}

	public ArrayList<Person> getPersons() {
		return persons;
	}
	
	public static void addRoomId(int id) {
		if(roomsId.size()>=1) {
			for(Integer i :roomsId ) {
				if(i==id) return;
			}
			roomsId.add(id);
			Collections.sort(roomsId);
		}
		else {
			roomsId.add(id);
		}
	}
	
	public void addRoom(Room room) {
		System.out.println("im here");
		rooms.add(room);
		addRoomId(room.getRoomID());
	}
	
	public ArrayList<Integer> getRoomsId() {
		return roomsId;
	}
	
	public ArrayList<Room> getRooms() {
		return rooms;
	}
}
