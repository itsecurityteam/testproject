package PaketFürEInProjekt;

import java.util.ArrayList;

public class Person {
     private Room currentRoom;
     private String Name;
     private  Status status;
     private ArrayList<Integer> visitedRooms = new ArrayList<Integer>();
//     private static int matricule;
     
     
	public Person(String name, Status status) {
		Name = name;
		this.status = status;
		this.currentRoom = null;
	}
     
     public void addVisitedRooms(int roomId) {
    	 visitedRooms.add(roomId);
    }
     
     public Room getCurrentRoom() {
		return currentRoom;
	}


     public String getName() {
		return Name;
	}

     public void setName(String name) {
		Name = name;
	}

     public Status getStatus() {
		return status;
	}

     public void setStatus(Status status) {
		this.status = status;
	}

     public void setCurrentRoom(Room currentRoom) {
		this.currentRoom = currentRoom;
	}

     public ArrayList<Integer> getVisitedRooms() {
 		return visitedRooms;
 	}

 
     public boolean getOutRoom(Room room){
    	  
    	 return room.removePerson(this);
     }
     
    
     
}
