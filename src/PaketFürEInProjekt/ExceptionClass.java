package PaketFürEInProjekt;
import java.util.HashMap;

/**
 * 
 * ExceptionClass als Singleton-Klasse, die in der hautpt (Kontroll-) Klasse anfsang einmalig instanziert wird:
 * Im Weiteren kann dann bei Fehlermeldungen immer auf diese Instanz und alle Fehlercodes zugegriffen werden
 *
 */
public final class ExceptionClass extends Exception{
	
	private static HashMap<Integer, String> errors;
	
	private static ExceptionClass instance;
	
	/*
	 * Im Singleton sind alle Konstruktoren private
	 */
	private ExceptionClass(){
		super();
		this.errors = new HashMap<Integer,String>();
		this.initialize();	 
	}
	
	private ExceptionClass(String message){
		super(message);
		this.errors = new HashMap<Integer,String>();
		this.initialize();	 
	}
	
	public synchronized static ExceptionClass getInstance(){
		if (instance == null) {
            instance = new ExceptionClass();
        }
        return instance;
	}	
	
	public void initialize(){
		 
		this.setError(302, "Error: You are already inside a room");
		this.setError(404, "Error: room not found");
		this.setError(502, "");
		
		// Weitere Exceptions (Martin):
		this.setError(601, "Error: Not a valid File");
		this.setError(602, "Error: Not a correct Timestamp");
	}
	
	public HashMap getErrors(){		
		return this.errors;
	}
    
	public String getError(int key){		
		return this.errors.get(key);
	}
	
	public void setError(int key , String value){
		this.errors.put(key, value);
	}
	

}

