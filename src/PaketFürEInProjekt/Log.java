package PaketFürEInProjekt;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author EM
 * Klasse zum Zwischenspeichern aller eingelesen Daten&Informationen
 *
 */
public class Log {
	private ExceptionClass exceptions = ExceptionClass.getInstance();
	
	private Aktion aktion;
	private int timestamp;
	private String token;
	private Status status;
	private Doing doing;
	private String name;// contains only {abc...xyzABC...XYZ}
	private int roomId;
	private String logName;

	private String bufferContent;
	
	
	public Log(Aktion aktion, int timestamp, String token, Status status, Doing doing, String name, int roomId, String logName){
		this.timestamp = timestamp;
		this.token = token;
		this.status = status;
		this.doing = doing;
		this.name = name;
		this.roomId = roomId;
		this.logName = logName;
	}
	
	public Log(Aktion aktion, int timestamp, String token, Status status, Doing doing, String name, String logName){
		this.timestamp = timestamp;
		this.token = token;
		this.status = status;
		this.doing = doing;
		this.name = name;
		this.logName = logName;
	}
	
	public Aktion getAktion(){
		return this.aktion;
	}
	
	public int getTimestamp(){
		return this.timestamp;
	}
	
	public String getToken(){
		return this.token;
	}
	
	public Status getStatus(){
		return this.status;
	}

	public Doing getDoing(){
		return this.doing;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getLogName(){
		return this.logName;
	}
	
	public String getBufferContent() {
    	return bufferContent;
	}
    
    public void addLineToBuffer(String line){
    	this.bufferContent += line + "\n";
    	//this.setBufferContent(this.bufferContent += line );
    }
	
	public void setBufferContent(String bufferContent) {
		
		this.bufferContent = bufferContent;
	}
	
	/**
     * read the file width name filename  and store the Content  in the bufferContent
     * @param String filename   to validate with matched format
     */
	 public void readLogFile(String filename){
		    //Ready Section 
	        
	        // This will reference one line at a time
	        String line = null;
	        String lastLine =null;

	        try {
	            // FileReader reads text files in the default encoding.
	            FileReader fileReader = new FileReader(filename);

	            // Always wrap FileReader in BufferedReader.
	            BufferedReader bufferedReader = new BufferedReader(fileReader);

	            while((line = bufferedReader.readLine()) != null) {  
	            	System.out.println(line);
	                this.addLineToBuffer(line);
	            }   

	            // Always close files.
	            bufferedReader.close();         
	        } catch(IOException ex) {
	 
	             ex.printStackTrace();
	        }
			 
	   }
	 
	 /**
     *  Store the Content of buffeContent on the file width the name filename
     * @param String filename   to validate with matched format
     */
	   public void wirteLogFile(String filename){
		   try {
	           // Assume default encoding.
	           FileWriter fileWriter = new FileWriter(filename);
	
	           // Always wrap FileWriter in BufferedWriter.
	           BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	
	           // Note that write() does not automatically
	           // append a newline character.
	           bufferedWriter.write(this.getBufferContent());
	           
	           //bufferedWriter.newLine();
	            
	           // Always close files.
	           bufferedWriter.close();
	       }
	       catch(IOException ex) {
	
	            ex.printStackTrace();
	       }
	   }
	/*
	public void setTimestamp(int time){
		File logFile = new File(this.getLogName());
		if (!logFile.canRead() || !logFile.isFile()){
			// Fehlermeldung einfügen
			System.out.println(exceptions.getError(601));
			System.exit(0);
		}
		BufferedReader in = null;
		String letzteZeile = null;
	     try {
	         in = new BufferedReader(new FileReader(logFile));
	         String zeile = null;
	         while ((zeile = in.readLine()) != null) {
	            letzteZeile = zeile;
	         }
	     } catch (IOException e) {
	         e.printStackTrace();
	     } finally {
	         if (in != null)
	             try {
	                 in.close();
	                } catch (IOException e) {
	                }
	        }
		this.timestamp = time;
	}
	*/
}
